function translateTileFields(labelDiv, translationFields) {
    // Ensure label is a string
    if (typeof labelDiv.textContent === "string") {
        // Convert the label's text content to uppercase for direct comparison
        const labelText = labelDiv.textContent.toUpperCase();
        // Check if the text content directly matches any key in the translation dictionary
        if (translationFields[labelText]) {
            // Replace the label's text content with the translation
            labelDiv.textContent = translationFields[labelText];
        }
    }
};

// DYNAMIC LANGUAGE DROP DOWN FEATURE
// Inject styles for the custom dropdown
    const customStyles = `
        .dropdown {
            position: relative;
            display: inline-block;
            z-index: 10000; /* High z-index to ensure it appears above other elements */
        }
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 125px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 10000; /* High z-index to ensure it appears above other elements */
            cursor: pointer;
        }
        .dropdown-content div {
            color: black;
            padding: 8px 12px;
            text-decoration: none;
            display: flex;
            align-items: center;
        }
        .dropdown-content div:hover {
            background-color: #f1f1f1;
        }
        .dropdown:hover .dropdown-content {
            display: block;
        }
        .dropdown-content img {
            margin-right: 10px;
            width: 24px;
            height: 18px;
        }
        .dropbtn {
            background-color: rgba(233,233,233);
            padding: 6.5px 10px;
            font-size: 14px;
            border: none;
            cursor: pointer;
            display: flex;
            width: 125px;  // Fixed width for consistent button size
            text-align: center;
            align-items: center;
            justify-content: center;  /* Add this line */
            font-weight: 600;
            margin-top: 4px;
        }
        .dropbtn img {
            margin-right: 5px;
            width: 24px;
            height: 18px;
        }
        .dropbtn:focus {
            outline: none;
        }
    `;
    $('head').append('<style>' + customStyles + '</style>');

    // Add the dropdown HTML
    const dropdownHtml = `
        <div class="dropdown">
            <button class="dropbtn">
                <img src="https://flagcdn.com/32x24/gb.png" alt="English">
                English
            </button>
            <div class="dropdown-content">
                <div data-lang="english">
                    <img src="https://flagcdn.com/32x24/gb.png" alt="English">
                    English
                </div>
                <div data-lang="chinese">
                    <img src="https://flagcdn.com/24x18/cn.png" alt="Chinese">
                    Chinese
                </div>
                <div data-lang="vietnamese">
                    <img src="https://flagcdn.com/24x18/vn.png" alt="Vietnamese">
                    Vietnamese
                </div>
            </div>
        </div>
    `;