//https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/mondeox/mondeox_dynamic_language.js

// ==UserScript==
// @name         DYNAMIC LANGUAGE OPTION
// @namespace    https://makeworkflow.de
// @description  Custom FLOW.PLUGIN to have more features on the MS DEVOPS board for increased visibility and usability
// @version      1.3.7
// @run-at       document-end
// @grant        unsafeWindow
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/jquery.min.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/waitForKeyElements.js
// @match        https://dev.azure.com/Mondeox/production.flow/_boards/board/t/*
// @match        https://dev.azure.com/Mondeox/production.flow/
// @match        https://dev.azure.com/Mondeox/planning.flow/_boards/board/t/*
// @match        https://dev.azure.com/Mondeox/planning.flow/
// ==/UserScript==

(function() {
    'use strict';

    // Inject styles for the custom dropdown
    const customStyles = `
        .dropdown {
            position: relative;
            display: inline-block;
            z-index: 10000; /* High z-index to ensure it appears above other elements */
        }
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 125px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 10000; /* High z-index to ensure it appears above other elements */
            cursor: pointer;
        }
        .dropdown-content div {
            color: black;
            padding: 8px 12px;
            text-decoration: none;
            display: flex;
            align-items: center;
        }
        .dropdown-content div:hover {
            background-color: #f1f1f1;
        }
        .dropdown:hover .dropdown-content {
            display: block;
        }
        .dropdown-content img {
            margin-right: 10px;
            width: 24px;
            height: 18px;
        }
        .dropbtn {
            background-color: rgba(233,233,233);
            padding: 6.5px 10px;
            font-size: 14px;
            border: none;
            cursor: pointer;
            display: flex;
            width: 125px;  // Fixed width for consistent button size
            text-align: center;
            align-items: center;
            justify-content: center;  /* Add this line */
            font-weight: 600;
            margin-top: 4px;
        }
        .dropbtn img {
            margin-right: 5px;
            width: 24px;
            height: 18px;
        }
        .dropbtn:focus {
            outline: none;
        }
    `;
    $('head').append('<style>' + customStyles + '</style>');

    // Add the dropdown HTML
    const dropdownHtml = `
        <div class="dropdown">
            <button class="dropbtn">
                <img src="https://flagcdn.com/32x24/gb.png" alt="English">
                English
            </button>
            <div class="dropdown-content">
                <div data-lang="english">
                    <img src="https://flagcdn.com/32x24/gb.png" alt="English">
                    English
                </div>
                <div data-lang="italian">
                    <img src="https://flagcdn.com/24x18/it.png" alt="Italian">
                    Italian
                </div>
            </div>
        </div>
    `;

    const headerTitleAreas = $('.bolt-header-title-area.flex-column.flex-grow.scroll-hidden');
    if (headerTitleAreas.length > 1) {
        $(headerTitleAreas[1]).after(dropdownHtml);
    }

    // Event listener for language selection
    $('.dropdown-content div').on('click', function() {
        const selectedLang = $(this).data('lang');
        const selectedText = $(this).text().trim();
        const selectedImgSrc = $(this).find('img').attr('src');
        setTranslationLayer(selectedLang);
        saveLanguageSetting(selectedLang); // Save the selected language

        // Update the button to show the selected value and image
        $('.dropbtn').html(`<img src="${selectedImgSrc}" alt="${selectedText}"> ${selectedText}`);
    });

    // Current translation layer
    let currentTranslationLayer = loadLanguageSetting();

    // Translation layers
    const translationLayers = {
        english: {
            "ORDER QTY": "ORDER QTY",
            "START_DATE": "START DATE",
            "ETD": "ETD",
            "_SIZE": "SIZE",
            "WIPQTY": "WIP QTY",
            "COLOR": "COLOR",
            "UPPER": "UPPER",
            "LINNING": "LINING",
            "SOLE": "SOLE",
            "PACKAGING": "PACKAGING",
            "OTHERS": "OTHERS",
            "SUBCONTRACTING": "SUBCONTRACTING"
        },
        italian: {
            "ORDER QTY": "QUANTITÀ ORDINE",
            "START_DATE": "DATA INIZIO",
            "ETD": "DATA CONS",
            "_SIZE": "TAGLIA",
            "WIPQTY": "WIP QTY",
            "COLOR": "COLORE",
            "INJECTS":"INIETTORI",
            "REJECTS":"RIFIUTO",
            "UPPER": "TOMAIA",
            "LINNING": "FODERA",
            "SOLE": "SUOLA",
            "PACKAGING": "IMBALLAGGIO",
            "OTHERS": "ALTRI",
            "SUBCONTRACTING": "SUBCONTRATTO"
        }
    };

    // Steps translation layer
    const stepsTranslationLayer = {
        english: {
            "NEW ORDER": "NEW ORDER",
            "INDUSTRALISATION": "INDUSTRIALISATION",
            "PLANNING": "PLANNING",
            "ETD/APPROVAL": "ETD/APPROVAL",
            "PULLRACK MATERIAL PURCHASING": "PULLRACK MATERIAL PURCHASING",
            "MATERIAL PURCHASING": "MATERIAL PURCHASING",
            "WAITING FOR MATERIAL": "WAITING FOR MATERIAL",
            "READY FOR PRODUCTION": "READY FOR PRODUCTION",
            "PULLRACK PRODUCTION": "PULLRACK PRODUCTION",
            "IN PRODUCTION": "IN PRODUCTION",
            "PULLRACK WAREHOUSING": "PULLRACK WAREHOUSING",
            "FINISHED GOODS WAREHOUSE": "FINISHED GOODS WAREHOUSE",
            "DONE": "DONE",
            "PULLRACK MATERIAL PREP": "PULLRACK MATERIAL PREP",
            "MATERIAL PREP": "MATERIAL PREP",
            "PULLRACK CUTTING": "PULLRACK CUTTING",
            "CUTTING": "CUTTING",
            "PULLRACK STITCHING PREP": "PULLRACK STITCHING PREP",
            "STITCHING PREP": "STITCHING PREP",
            "PULLRACK STITCHING": "PULLRACK STITCHING",
            "STITCHING": "STITCHING",
            "PULLRACK LASTING": "PULLRACK LASTING",
            "LASTING": "LASTING",
            "PULLRACK PACKING": "PULLRACK PACKING",
            "PACKING": "PACKING",
            "WAREHOUSE": "WAREHOUSE",
            "SHIPPED": "SHIPPED"
        },
        italian: {
            "NEW ORDER": "NUOVO ORDINE",
            "INDUSTRALISATION": "INDUSTRIALIZZAZIONE",
            "PLANNING": "PIANIFICAZIONE",
            "ETD/APPROVAL": "DATA CONSEGNA/APPROVAZIONE",
            "PULLRACK MATERIAL PURCHASING": "IN ATTESA DELL'ACQUISTO MATERIALE",
            "MATERIAL PURCHASING": "ACQUISTO MATERIALE",
            "WAITING FOR MATERIAL": "IN ATTESA DEL MATERIALE",
            "READY FOR PRODUCTION": "PRONTO PER LA PRODUZIONE",
            "PULLRACK PRODUCTION": "IN ATTESA DEL PRODUZIONE",
            "IN PRODUCTION": "IN PRODUZIONE",
            "PULLRACK WAREHOUSING": "IN ATTESA DEL MAGAZZINO",
            "FINISHED GOODS WAREHOUSE": "MAGAZZINO PRODOTTI FINITI",
            "DONE": "FINALIZZATO",
            "PULLRACK MATERIAL PREP": "IN ATTESA DELLA PREPARAZIONE",
            "MATERIAL PREP": "PREPARAZIONE",
            "PULLRACK CUTTING": "IN ATTESA DEL TAGLIO",
            "CUTTING": "TAGLIO",
            "PULLRACK STITCHING PREP": "IN ATTESA DELLA PREPARAZIONE DELL'ORLATURA",
            "STITCHING PREP": "PREPARAZIONE DELL'ORLATURA",
            "PULLRACK STITCHING": "IN ATTESA DELL'ORLATURA",
            "STITCHING": "ORLATURA",
            "PR PU INJECTION":"IN ATESSA DELL'INIEZIONE DI PU",
            "PU INJECTION":"INIEZIONE DI PU",
            "PULLRACK LASTING": "IN ATTESA DEL MONTAGGIO",
            "LASTING": "MONTAGGIO",
            "PULLRACK PACKING": "IN ATTESA DEL FINISSAGGIO",
            "PACKING": "FINISSAGGIO",
            "WAREHOUSING": "MAGAZZINO",
            "SHIPPED": "SPEDITO"
        }
    };

    function saveLanguageSetting(language) {
        localStorage.setItem('selectedLanguage', language);
    }

    function loadLanguageSetting() {
        return localStorage.getItem('selectedLanguage') || 'english';
    }

    // Function to set the translation layer
    function setTranslationLayer(language) {
        currentTranslationLayer = language;
        refreshTranslations();
    }

    // Function to refresh translations
    function refreshTranslations() {
        const fields = $('div.fields').find('div.label.text-ellipsis');
        fields.each(function() {
            const labelDiv = this;
            translateTileFields(labelDiv, translationLayers[currentTranslationLayer]);
        });

        const headers = $('.kanban-board-column-header');
        headers.each(function() {
            const header = $(this);
            translateHeaderSteps(header, stepsTranslationLayer[currentTranslationLayer]);
        });
    }

    // FIELDS ON WORKITEM FOUND
    function fieldsOnCardFound(jNode) {
        $('.value, .editor-component').css('text-align', 'right');
        $('.label').css('overflow', 'visible');

        const shouldTranslate = translationLayers[currentTranslationLayer] && Object.keys(translationLayers[currentTranslationLayer]).length > 0;

        const labelDivs = jNode[0].querySelectorAll('div.label.text-ellipsis');

        labelDivs.forEach(function(labelDiv) {
            if (shouldTranslate) {
                translateTileFields(labelDiv, translationLayers[currentTranslationLayer]);
            }
        });
    }

    // Add dropdown to the document once
    waitForKeyElements('div.fields', fieldsOnCardFound);

    function translateTileFields(labelDiv, translationFields) {
        if (typeof labelDiv.textContent === "string") {
            if (!labelDiv.getAttribute('data-original-text')) {
                labelDiv.setAttribute('data-original-text', labelDiv.textContent);
            }
            const originalText = labelDiv.getAttribute('data-original-text').toUpperCase();
            if (translationFields[originalText]) {
                labelDiv.textContent = translationFields[originalText];
            }
        }
    }

    function translateHeaderSteps(header, translationFields) {
        const ariaLabel = header.attr('aria-label');
        if (ariaLabel) {
            let span = header.find('span').filter(function() {
                return $(this).text().toUpperCase() === ariaLabel.toUpperCase();
            }).first(); // Find the first matching span

            if (!span.length) {
                // If no span matches the aria-label, check against data-original-text
                span = header.find('span').filter(function() {
                    return $(this).attr('data-original-text') && $(this).attr('data-original-text').toUpperCase() === ariaLabel.toUpperCase();
                }).first();
            }

            if (span.length) {
                if (!span.attr('data-original-text')) {
                    span.attr('data-original-text', span.text());
                }
                const originalText = span.attr('data-original-text').toUpperCase();
                if (translationFields[originalText]) {
                    span.text(translationFields[originalText]);
                }
            }
        }
    }
    $(document).ready(function() {
        const selectedLang = loadLanguageSetting();
        const selectedText = selectedLang === 'italian' ? 'Italian' : 'English';
        const selectedImgSrc = selectedLang === 'italian' ? 'https://flagcdn.com/24x18/it.png' : 'https://flagcdn.com/32x24/gb.png';
        $('.dropbtn').html(`<img src="${selectedImgSrc}" alt="${selectedText}"> ${selectedText}`);
        setTranslationLayer(selectedLang);  // Set the initial translation layer
    });

    // Wait for the kanban board column headers to be available
    waitForKeyElements('.kanban-board-column-header', function() {
        refreshTranslations();
    });

})();
