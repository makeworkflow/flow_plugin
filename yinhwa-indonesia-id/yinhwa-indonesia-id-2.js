//// RAW LINK FOR DIRECT COPY: https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/yinhwa-indonesia-id/yinhwa-indonesia-id-2.js


// ==UserScript==
// @name         Flow.Plugin 2 - YinHwa Indonesia - ID
// @namespace    https://makeworkflow.de
// @description  Custom FLOW.PLUGIN to have more features on the MS DEVOPS board for increased visibility and usability
// @version      0.1.7
// @run-at       document-end
// @grant        unsafeWindow

// @match        https://dev.azure.com/YinHwa-Indonesia/PRODUCTION
// @match        https://dev.azure.com/YinHwa-Indonesia/PRODUCTION/_boards/board/t/*
// @match        https://dev.azure.com/YinHwa-Indonesia/MODEL%20ROOM
// @match        https://dev.azure.com/YinHwa-Indonesia/MODEL%20ROOM/_boards/board/t/*

// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/jquery.min.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/waitForKeyElements.js

// ==/UserScript==

(function() {
    'use strict';

    // DYNAMIC LANGUAGE DROP DOWN FEATURE
    // Inject styles for the custom dropdown
    const customStyles = `
        .dropdown {
            position: relative;
            display: inline-block;
            z-index: 10000; /* High z-index to ensure it appears above other elements */
        }
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 125px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 10000; /* High z-index to ensure it appears above other elements */
            cursor: pointer;
        }
        .dropdown-content div {
            color: black;
            padding: 8px 12px;
            text-decoration: none;
            display: flex;
            align-items: center;
        }
        .dropdown-content div:hover {
            background-color: #f1f1f1;
        }
        .dropdown:hover .dropdown-content {
            display: block;
        }
        .dropdown-content img {
            margin-right: 10px;
            width: 26px;
            height: 20px;
        }
        .dropbtn {
            background-color: rgba(233,233,233);
            padding: 6.5px 10px;
            font-size: 14px;
            border: none;
            cursor: pointer;
            display: flex;
            width: 135px;  // Fixed width for consistent button size
            text-align: center;
            align-items: center;
            justify-content: center;  /* Add this line */
            font-weight: 600;
            margin-top: 4px;
        }
        .dropbtn img {
            margin-right: 5px;
            width: 26px;
            height: 20px;
        }
        .dropbtn:focus {
            outline: none;
        }
    `;
    $('head').append('<style>' + customStyles + '</style>');

    // Add the dropdown HTML
    const dropdownHtml = `
        <div class="dropdown">
            <button class="dropbtn">
                <img src="https://flagcdn.com/64x48/gb.png" alt="English">
                English
            </button>
            <div class="dropdown-content">
                <div data-lang="english">
                    <img src="https://flagcdn.com/64x48/gb.png" alt="English">
                    English
                </div>
                <div data-lang="chinese">
                    <img src="https://flagcdn.com/64x48/cn.png" alt="Chinese">
                    Chinese
                </div>
                <div data-lang="indonesian">
                    <img src="https://flagcdn.com/64x48/id.png" alt"Indonesian">
                    Indonesian
            </div>
        </div>
    `;

    const headerTitleAreas = $('.bolt-header-title-area.flex-column.flex-grow.scroll-hidden');
    if (headerTitleAreas.length > 1) {
        $(headerTitleAreas[1]).after(dropdownHtml);
    }

    // Event listener for language selection
    $('.dropdown-content div').on('click', function() {
        const selectedLang = $(this).data('lang');
        const selectedText = $(this).text().trim();
        const selectedImgSrc = $(this).find('img').attr('src');
        setTranslationLayer(selectedLang);
        saveLanguageSetting(selectedLang); // Save the selected language

        // Update the button to show the selected value and image
        $('.dropbtn').html(`<img src="${selectedImgSrc}" alt="${selectedText}"> ${selectedText}`);
    });

    // Current translation layer
    let currentTranslationLayer = loadLanguageSetting();

    // Translation layers
    const translationLayers = {
        english: {
            "ORDER DATE": "ORDER DATE",
            "CUSTOMER": "CUSTOMER",
            "BRAND": "BRAND",
            "MODEL NUMBER": "MODEL NO",
            "LAST CODE": "LAST CODE",
            "SIZE_": "SIZE",
            "ORDER QTY": "ORDER QTY",
            "WIP QTY": "WIP QTY",
            "ETD": "ETD"
        },
        chinese: {
            "ORDER DATE": "订单日期",
            "CUSTOMER": "客户",
            "BRAND": "品牌",
            "MODEL NUMBER": "模子编号",
            "LAST CODE": "楦头编号",
            "SIZE_": "码数",
            "ORDER QTY": "订单数量",
            "WIP QTY": "本站数量",
            "ETD": "交货日期"
        },
        indonesian: {
            "ORDER DATE": "TGL PESANAN",
            "CUSTOMER": "PELANGGAN",
            "BRAND": "MEREK",
            "MODEL NUMBER": "NOMOR MODEL",
            "LAST CODE": "KODE LASTE",
            "SIZE_": "UKURAN",
            "ORDER QTY": "JUMLAH PESANAN",
            "WIP QTY": "JUMLAH WIP",
            "ETD": "ETD"
        },
    };

    // Updated translation array
    const stepsTranslationLayer = {
        english: {
            "ORDER APPROVAL": "ORDER APPROVAL",
            "PREPARATION": "PREPARATION",
            "PR MODEL MAKING": "PR MODEL MAKING",
            "MANUAL MODEL MAKING": "MANUAL MODEL MAKING",
            "PR CAD 2D": "PR CAD 2D",
            "CAD 2D": "CAD 2D",
            "PR CAD 3D & DIGITALISATION": "PR CAD 3D & DIGITALISATION",
            "CAD 3D & DIGITALISATION": "CAD 3D & DIGITALISATION",
            "MR APPROVAL": "MR APPROVAL",
            "DONE": "DONE",
            "SALES APPROVAL": "SALES APPROVAL",
            "PR PRODUCTION": "PR PRODUCTION",
            "PR ROUGHING": "PR ROUGHING",
            "ROUGHING": "ROUGHING",
            "PR CUTTING": "PR CUTTING",
            "CUTTING": "CUTTING",
            "PR MOD": "PR MOD",
            "MOD": "MOD",
            "PR SDF": "PR SDF",
            "SDF": "SDF",
            "PR HEEL CUTTING": "PR HEEL CUTTING",
            "HEEL CUTTING": "HEEL CUTTING",
            "PR THIMBLE": "PR THIMBLE",
            "THIMBLE": "THIMBLE",
            "PR ASSEMBLY AND FINISHING": "PR ASSEMBLY & FINISHING",
            "ASSEMBLY AND FINISHING": "ASSEMBLY & FINISHING",
            "PR STAMPING": "PR STAMPING",
            "STAMPING": "STAMPING",
            "PR PIN MARK": "PR PIN MARK",
            "PIN MARK": "PIN MARK",
            "PR QC": "PR QC",
            "QC": "QC",
            "PR PACKING": "PR PACKING",
            "PACKING": "PACKING",
            "FG WAREHOUSE": "FG WAREHOUSE",
            "SHIPPED": "SHIPPED",
        },        
        chinese: {
            "ORDER APPROVAL": "订单确认",
            "PREPARATION": "准备纸板",
            "PR MODEL MAKING": "待模子制作",
            "MANUAL MODEL MAKING": "模子制作中",
            "PR CAD 2D": "待2D作业",
            "CAD 2D": "2D作业中",
            "PR CAD 3D & DIGITALISATION": "待3D作业和扫描",
            "CAD 3D & DIGITALISATION": "3D作业和扫描",
            "MR APPROVAL": "模具室审核",
            "DONE": "完成",
"SALES APPROVAL": "业务确认",
            "PR PRODUCTION": "待生产",
            "PR ROUGHING": "待粗车",
            "ROUGHING": "粗车",
            "PR CUTTING": "待结合",
            "CUTTING": "结合",
            "PR MOD": "待传统细车",
            "MOD": "传统细车",
            "PR SDF": "待SDF细车",
            "SDF": "SDF细车",
            "PR HEEL CUTTING": "待切统",
            "HEEL CUTTING": "切统",
            "PR THIMBLE": "待打座子",
            "THIMBLE": "打座子",
            "PR ASSEMBLY AND FINISHING": "待组装和磨头尾",
            "ASSEMBLY AND FINISHING": "组装和磨头尾",
            "PR STAMPING": "待盖印",
            "STAMPING": "盖印",
            "PR PIN MARK": "待做记号",
            "PIN MARK": "做记号",
            "PR QC": "待品检",
            "QC": "品检",
            "PR PACKING": "待包装",
            "PACKING": "完成包装",
            "FG WAREHOUSE": "成品仓库",
            "SHIPPED": "完成出货",
        },
        indonesian: {
            "ORDER APPROVAL": "PERSETUJUAN ORDER",
            "PREPARATION": "PERSIAPAN",
            "PR MODEL MAKING": "PR PEMBUATAN MODEL",
            "MANUAL MODEL MAKING": "PEMBUATAN MODEL MANUAL",
            "PR CAD 2D": "PR 2D CAD",
            "CAD 2D": "2D CAD",
            "PR CAD 3D & DIGITALISATION": "PR 3D CAD & DIGITALISASI",
            "CAD 3D & DIGITALISATION": "3D CAD & DIGITALISASI",
            "MR APPROVAL": "PERSETUJUAN MR",
            "DONE": "SELESAI",
            "SALES APPROVAL": "PERSETUJUAN SALES",
            "PR PRODUCTION": "PR PRODUKSI",
            "PR ROUGHING": "PR ROUGHING",
            "ROUGHING": "ROUGHING",
            "PR CUTTING": "PR CUTTING",
            "CUTTING": "CUTTING",
            "PR MOD": "PR MOD",
            "MOD": "MOD",
            "PR SDF": "PR SDF",
            "SDF": "SDF",
            "PR HEEL CUTTING": "PR HEEL CUTTING",
            "HEEL CUTTING": "HEEL CUTTING",
            "PR THIMBLE": "PR THIMBLE",
            "THIMBLE": "THIMBLE",
            "PR ASSEMBLY AND FINISHING": "PR ASSEMBLY & FINISHING",
            "ASSEMBLY AND FINISHING": "ASSEMBLY & FINISHING",
            "PR STAMPING": "PR STAMPLE",
            "STAMPING": "STAMPLE",
            "PR PIN MARK": "PR PENAMBAHAN PIN",
            "PIN MARK": "PENAMBAHAN PIN",
            "PR QC": "PR QC",
            "QC": "QC",
            "PR PACKING": "PR PENGEMASAN",
            "PACKING": "PENGEMASAN",
            "FG WAREHOUSE": "GUDANG BARANG JADI",
            "SHIPPED": "PENGIRIMAN",
        }
    };

    function saveLanguageSetting(language) {
        localStorage.setItem('selectedLanguage', language);
    }

    function loadLanguageSetting() {
        return localStorage.getItem('selectedLanguage') || 'english';
    }

    // Function to set the translation layer
    function setTranslationLayer(language) {
        currentTranslationLayer = language;
        refreshTranslations();
    }

    // Function to refresh translations
    function refreshTranslations() {
        const fields = $('div.fields').find('div.label.text-ellipsis');
        fields.each(function() {
            const labelDiv = this;
            translateTileFields(labelDiv, translationLayers[currentTranslationLayer]);
        });

        const headers = $('.kanban-board-column-header');
        headers.each(function() {
            const header = $(this);
            translateHeaderSteps(header, stepsTranslationLayer[currentTranslationLayer]);
        });
    }

    // FIELDS ON WORKITEM FOUND
    function fieldsOnCardFound(jNode) {
        $('.value, .editor-component').css('text-align', 'right');
        $('.label').css('overflow', 'visible');

        const shouldTranslate = translationLayers[currentTranslationLayer] && Object.keys(translationLayers[currentTranslationLayer]).length > 0;

        const labelDivs = jNode[0].querySelectorAll('div.label.text-ellipsis');

        labelDivs.forEach(function(labelDiv) {
            if (shouldTranslate) {
                translateTileFields(labelDiv, translationLayers[currentTranslationLayer]);
            }
        });
    }

    // Add dropdown to the document once
    waitForKeyElements('div.fields', fieldsOnCardFound);

    function translateTileFields(labelDiv, translationFields) {
        if (typeof labelDiv.textContent === "string") {
            if (!labelDiv.getAttribute('data-original-text')) {
                labelDiv.setAttribute('data-original-text', labelDiv.textContent);
            }
            const originalText = labelDiv.getAttribute('data-original-text').toUpperCase();
            if (translationFields[originalText]) {
                labelDiv.textContent = translationFields[originalText];
            }
        }
    }

    function translateHeaderSteps(header, translationFields) {
        const ariaLabel = header.attr('aria-label');
        if (ariaLabel) {
            let span = header.find('span').filter(function() {
                return $(this).text().toUpperCase() === ariaLabel.toUpperCase();
            }).first(); // Find the first matching span

            if (!span.length) {
                // If no span matches the aria-label, check against data-original-text
                span = header.find('span').filter(function() {
                    return $(this).attr('data-original-text') && $(this).attr('data-original-text').toUpperCase() === ariaLabel.toUpperCase();
                }).first();
            }

            if (span.length) {
                if (!span.attr('data-original-text')) {
                    span.attr('data-original-text', span.text());
                }
                const originalText = span.attr('data-original-text').toUpperCase();
                if (translationFields[originalText]) {
                    span.text(translationFields[originalText]);
                }
            }
        }
    }

    $(document).ready(function() {
        const languages = {
        'english': {
            text: 'English',
            imgSrc: 'https://flagcdn.com/64x48/gb.png'
        },
        'chinese': {
            text: 'Chinese',
            imgSrc: 'https://flagcdn.com/64x48/cn.png'
        },
        'indonesian': {
            text: 'Indonesian',
            imgSrc: 'https://flagcdn.com/64x48/id.png'
        }

    };
        const selectedLang = loadLanguageSetting();
        const selected = languages[selectedLang] || languages.English; // Default to English if not found

        $('.dropbtn').html(`<img src="${selected.imgSrc}" alt="${selected.text}"> ${selected.text}`);
        setTranslationLayer(selectedLang); // Set the initial translation layer
    });

    // Wait for the kanban board column headers to be available
    waitForKeyElements('.kanban-board-column-header', function() {
        refreshTranslations();
    });

})();
