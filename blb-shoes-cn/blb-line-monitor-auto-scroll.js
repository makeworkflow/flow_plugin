// ==UserScript==
// @name         Line Monitor Auto Scroll & Page Info
// @namespace    http://tampermonkey.net/
// @version      1.0.3
// @description  Automatically scrolls the gallery and adds page indicators below "Total Orders"
// @match        https://runtime-app.powerplatform.com/publishedapp/*
// @match        https://apps.powerapps.com/play/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const GALLERY_SELECTOR = '.virtualized-gallery.hideScrollbar';
    const TOTAL_ORDERS_SELECTOR = 'pre.___hfsv440'; // Replace this with the exact selector of the "Total Orders" element
    const ORDER_HEIGHT = 140; // Pixels per order
    const ORDERS_PER_SCREEN = 4; // Number of orders visible per screen
    const INTERVAL = 10000; // 10 seconds in milliseconds

    let gallery = null;
    let totalOrdersElement = null;
    let scrollInterval = null;
    let dynamicPageNumbers = null
    let totalOrders = 0;
    let totalScrollSteps = 0;
    let currentScrollStep = 0;
    let fullScrollSteps = 0;
    let remainingScroll = 0;
    let totalPages = 0;
    let currentPage = 1;

    /**
     * Function to extract the total number of orders from the UI
     * @returns {number} - Total number of orders
     */
    function getTotalOrders() {
        const totalOrdersElements = document.querySelectorAll(TOTAL_ORDERS_SELECTOR);
        if (totalOrdersElements.length < 2) {
            // console.error("Total Orders element not found.");
            return 0;
        }

        // Select the second occurrence
        const totalOrdersElement = totalOrdersElements[1];

        // Extract and parse the total orders value
        const ordersText = totalOrdersElement.textContent;
        const ordersNumber = ordersText ? parseInt(ordersText, 10) : 0;

        // console.log("Total Orders:", ordersNumber);
        return ordersNumber;
    }

    /**
     * Function to calculate the total number of pages based on total orders
     */
    function calculateScrollSteps() {
        if (totalOrders === 0) return;

        const totalHeight = totalOrders * ORDER_HEIGHT;
        const visibleHeight = ORDERS_PER_SCREEN * ORDER_HEIGHT;
        const scrollableHeight = totalHeight - visibleHeight;

        if (scrollableHeight <= 0) {
            totalScrollSteps = 0;
            fullScrollSteps = 0;
            remainingScroll = 0;
            totalPages = 1;
            currentPage = 1;
            // console.log("All orders fit within one screen. No scrolling required.");
            updatePageIndicator();
            return;
        }

        fullScrollSteps = Math.floor(scrollableHeight / (ORDERS_PER_SCREEN * ORDER_HEIGHT));
        remainingScroll = scrollableHeight % (ORDERS_PER_SCREEN * ORDER_HEIGHT);

        totalScrollSteps = fullScrollSteps + (remainingScroll > 0 ? 1 : 0);
        totalPages = totalScrollSteps + 1; // Including the first page
        currentPage = 1;

        // console.log(`Total Pages: ${totalPages}`);
        updatePageIndicator();
    }

    function createPageIndicator() {
        let pageIndicator = document.querySelector('#page-indicator');
        if (!pageIndicator) {
            pageIndicator = document.createElement('div');
            pageIndicator.id = 'page-indicator';
            pageIndicator.style.fontSize = '25px';
            pageIndicator.style.color = 'white';
            pageIndicator.style.zIndex = '100000';
            pageIndicator.style.position = 'fixed';
            pageIndicator.style.bottom = '5px';
            pageIndicator.style.right = '60px';
            pageIndicator.style.fontWeight = '600';
            pageIndicator.style.whiteSpace = 'pre-line';
            pageIndicator.style.lineHeight = '1.5';
            pageIndicator.style.textAlign = 'center';
            pageIndicator.style.fontFamily = 'Verdana, sans-serif';
            totalOrdersElement.parentNode.appendChild(pageIndicator);

            // Create a static "Page:" label
            const staticLabel = document.createElement('span');
            staticLabel.textContent = 'Page:';
            staticLabel.style.display = 'block'; // Ensure it stays on its own line
            pageIndicator.appendChild(staticLabel);

            // Create a dynamic part for the page numbers
            dynamicPageNumbers = document.createElement('span');
            dynamicPageNumbers.id = 'dynamic-page-numbers';
            pageIndicator.appendChild(dynamicPageNumbers);

            dynamicPageNumbers.textContent = "1/1";

            totalOrdersElement.parentNode.appendChild(pageIndicator);
        }
    }

    /**
     * Function to create or update the "Page: 1/2" indicator below "Total Orders"
     */
    function updatePageIndicator() {
        const totalOrdersElement = document.querySelector(TOTAL_ORDERS_SELECTOR);
        if (!totalOrdersElement) {
            // console.error("Total Orders element not found for page indicator.");
            return;
        }

        createPageIndicator();

        // Update only the dynamic page numbers
        const dynamicPageNumbers = document.querySelector('#dynamic-page-numbers');
        if (dynamicPageNumbers) {
            dynamicPageNumbers.textContent = `${currentPage}/${totalPages}`;
        }
    }

    /**
     * Function to start the scrolling process
     */
    function startScrolling() {
        if (!gallery || !totalOrdersElement) {
            console.error("Gallery and totalOrders elements not found.");
            return;
        }

        totalOrders = getTotalOrders();
        calculateScrollSteps();

        if (scrollInterval) {
            clearInterval(scrollInterval); // Clear any existing intervals
        }

        scrollInterval = setInterval(() => {
            if (!gallery || !totalOrdersElement) return;

            const newTotalOrders = getTotalOrders();

            // If total orders have changed, recalculate and reset
            if (newTotalOrders !== totalOrders) {
                // console.log("Detected change in total orders.");
                totalOrders = newTotalOrders;
                calculateScrollSteps();
                gallery.scrollTo({ top: 0, behavior: 'smooth' });
                // console.log("Resetting scroll due to updated total orders.");
                return;
            }

            if (totalScrollSteps === 0) {
                // console.log("No scrolling required.");
                updatePageIndicator();
                return;
            }

            if (currentScrollStep < fullScrollSteps) {
                // Scroll by full screen height
                const scrollAmount = ORDERS_PER_SCREEN * ORDER_HEIGHT;
                gallery.scrollBy({ top: scrollAmount, behavior: 'smooth' });
                currentScrollStep++;
                currentPage++;
                // console.log(`Scrolling to page ${currentPage}/${totalPages}`);
                updatePageIndicator();
            } else if (currentScrollStep === fullScrollSteps && remainingScroll > 0) {
                // Scroll by the remaining scroll amount
                gallery.scrollBy({ top: remainingScroll, behavior: 'smooth' });
                currentScrollStep++;
                currentPage++;
                // console.log(`Scrolling to final page ${currentPage}/${totalPages}`);
                updatePageIndicator();

                setTimeout(() => {
                    gallery.scrollTo({ top: 0, behavior: 'smooth' });
                    // console.log("Resetting to top after final page.");
                    currentScrollStep = 0;
                    currentPage = 1;
                    updatePageIndicator();
                }, INTERVAL / 2);
            } else {
                gallery.scrollTo({ top: 0, behavior: 'smooth' });
                // console.log("All pages completed. Resetting to top.");
                currentScrollStep = 0;
                currentPage = 1;
                updatePageIndicator();
            }
        }, INTERVAL);
    }

    /**
     * MutationObserver to detect when the gallery element is added to the DOM
     */
    const observer = new MutationObserver(() => {
        if (!gallery || !totalOrdersElement) {
            gallery = document.querySelector(GALLERY_SELECTOR);

            const totalOrdersElements = document.querySelectorAll(TOTAL_ORDERS_SELECTOR);
            if (totalOrdersElements.length >= 2) {
                totalOrdersElement = totalOrdersElements[1];
                console.log(totalOrdersElement.textContent);
            } else {
                totalOrdersElement = null;
            }
        }
        if (gallery && totalOrdersElement) {
            const checkInterval = setInterval(() => {
                const totalOrders = getTotalOrders()
                if (totalOrders !== 0) {
                    // console.log("Gallery and totalOrders elements found, mutationObserver.");
                    startScrolling(); // Start scrolling.
                    clearInterval(checkInterval)
                    observer.disconnect();
                } else {
                    createPageIndicator();
                    // console.log("Total Orders is 0. Waiting for update...");
                }
            }, 400);
        }
    });

    // Observe DOM changes
    observer.observe(document.body, { childList: true, subtree: true });

})();