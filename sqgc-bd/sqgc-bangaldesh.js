/////// RAW LINK FOR DIRECT COPY: https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/sqgc-bd/sqgc-bangaldesh.js
// ==UserScript==
// @name         Flow.Plugin - SQGC Bangladesh - BD
// @description  FLOW.PLUGIN to have custom features on the MS DEVOPS board for increased visability and usability
// @namespace    https://makeworkflow.de
// @run-at       document-end
// @grant        unsafeWindow
// @version      0.0.1

// @match        https://dev.azure.com/SQGC/*
// @match        https://dev.azure.com/SQGC/SQGC/_boards/board/t/*

// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/jquery.min.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/waitForKeyElements.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/planningFlowLogic.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/translationsLogic.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/swimlaneFlagpoleColorLogic.js

// ==/UserScript==

(function() {
    'use strict';
    // get tampermonkey version as variable
    var version = GM_info.script.version;

    // ON IMAGE FIELD FOUND
    let imageFieldLabel = 'div.label.text-ellipsis:contains("IMAGE URL")';

    // ON IMAGE UPDATE
    let onOpenItemImageFieldElement = 'input#__bolt-IMAGE-URL-input';
    let onOpenItemWorkItemIDElement = 'div.flex-row.body-xl.padding-bottom-4.padding-right-8.flex-center';

    // SET DATES TO LOCALE
    let onOpenItemDates = 'time.bolt-time-item.white-space-nowrap';

    // SET FIELDS ON CARD DIV
    let fieldsOnCardDiv = 'div.fields';

    // SET AMOUNT OF MINUTES THE PAGE SHOULD RELOAD
    const minutes = 240;

    /* ##### ##### #### #### ### ### ##
     ----- PLANNING FLOW SETTINGS -----
    ##### ##### #### #### ### ### ## */

    // traffic light settings per percentage
    const trafficLightSettings = {
        // same or higher => %
        green: 100,
        // same or higher  => %
        orange: 26,
        // only lower < %
        red: 26
    }

    // PLANNING FLOW FIELDS TO ADD TRAFFIC LIGHT TO
    const planningFlowFields = []
    /* ----- ######## #### ######## ---- */


        // WORDS TO TRANSLATE
   const translationFields = {};

    // ExtensionCache DB setup
    const ongoingCaching = new Map();
    const blobURLCache = new Map();
    const dbName = "Flow.Plugin: " + version;
    const storeName = "images";
    let db;

    // OPEN DB CONNECTION
    const openDB = () => {
        return new Promise((resolve, reject) => {
            const request = indexedDB.open(dbName, 1);

            request.onupgradeneeded = function(event) {
                db = event.target.result;
                if (!db.objectStoreNames.contains(storeName)) {
                    db.createObjectStore(storeName, { keyPath: "url" });
                }
            };

            request.onsuccess = function(event) {
                db = event.target.result;
                resolve(db);
            };

            request.onerror = function(event) {
                console.error("IndexedDB error:", event.target.errorCode);
                reject(event.target.errorCode);
            };
        });
    };

    // Initialize IndexedDB
    openDB();

    // CACHE IMAGE
    const cacheImage = (url, callback) => {
        fetch(url)
            .then(response => {
            if (!response.ok) {
                throw new Error(`Network response was not ok, status: ${response.status}`);
            }

            // Check if the content type of the response is suitable for blob conversion
            const contentType = response.headers.get("content-type");
            if (!contentType || !contentType.includes("image")) {
                throw new Error(`Response is not an image, content type: ${contentType}`);
            }

            return response.blob();
        })
            .then(blob => {
            // Blob conversion should be successful here
            const transaction = db.transaction([storeName], "readwrite");
            const store = transaction.objectStore(storeName);
            var putRequest = store.put({ url: url, data: blob });

            putRequest.onsuccess = function() {
                // console.log("Image successfully cached in IndexDB and saved to blob url cache");
                const URLObject = window.URL || window.webkitURL;
                const imageURL = URLObject.createObjectURL(blob);
                blobURLCache.set(url, imageURL);

                if (typeof callback === "function") {
                    callback(imageURL);
                }
            };

            putRequest.onerror = function(event) {
                console.log("Error caching image:", event.target.error);
            };
        })
            .catch(error => console.error('Error during fetching and caching:', error));
    };

    const retrieveImage = (url, callback) => {
        if (ongoingCaching.has(url)) {
            ongoingCaching.get(url).then(() => {
                if (blobURLCache.has(url)) {
                    // console.log("Image found in blob cache, using blob url cache");
                    callback(blobURLCache.get(url));
                }
            });
            return;
        }

        // Create a promise representing the caching operation
        const cachingPromise = new Promise((resolve, reject) => {
            const transaction = db.transaction([storeName], "readonly");
            const store = transaction.objectStore(storeName);
            const request = store.get(url);

            request.onsuccess = function(event) {
                if (event.target.result) {
                    // console.log("Image found in IndexedDB cache, converted to blob and saved in blob url cache");
                    const URLObject = window.URL || window.webkitURL;
                    const imageURL = URLObject.createObjectURL(event.target.result.data);
                    blobURLCache.set(url, imageURL);
                    callback(imageURL);
                    resolve();
                } else {
                    // console.log("Image not found in indexedDB cache, caching now.");
                    cacheImage(url, (cachedSrc) => {
                        callback(cachedSrc);
                        resolve();
                    });
                }
            };

            request.onerror = function(event) {
                console.error('Error in retrieving from indexedDB cache:', event);
                reject();
            };
        });

        // Add the caching operation to the map
        ongoingCaching.set(url, cachingPromise);

        // Once caching is complete, remove it from the map
        cachingPromise.finally(() => {
            ongoingCaching.delete(url);
        });
    };

    // TEST IF URL IS VALID IMAGE
    function testImageUrl(url) {
        return new Promise(function(resolve, reject) {
            var image = new Image();

            image.onload = function() {
                resolve(true);
            };

            image.onerror = function() {
                resolve(false);
            };

            image.src = url;
        });
    }

    function updateImageElement(imageElement, src, jNode) {
        if (!imageElement.length) {
            let img = document.createElement("img");
            img.src = src;
            img.width = 75;
            img.height = 75;
            img.style.marginRight = "auto";
            img.className = "workItemPictures";
            jNode.parent().parent().prev().children(':first').prepend(img);
        } else {
            imageElement.attr('src', src);
        }
        jNode.parent().hide();
    }

    // ON IMAGE FIELD FOUND
    function onImageFieldFound(jNode) {
        var imageURL = jNode.next().text();
        var image = jNode.parent().parent().find("img");

        if (!imageURL || !testImageUrl(imageURL)) {
            // console.log("Invalid or empty IMAGE URL, skipping fetch and cache.");
            image.remove();
            return;
        }

        retrieveImage(imageURL, function(cachedSrc) {
            if (cachedSrc) {
                updateImageElement(image, cachedSrc, jNode);
            } else {
                cacheImage(imageURL, function(blob) {
                    const URLObject = window.URL || window.webkitURL;
                    const imageURL = URLObject.createObjectURL(blob);
                    updateImageElement(image, imageURL, jNode);
                });
            }
        });
    }
    waitForKeyElements(imageFieldLabel, onImageFieldFound);

    // ON IMAGE FIELD UPDATE
    function onImageFieldUpdate(jNode) {
        // FORCE UPDATE BEFORE SHOWING FIELDS
        autoUpdate();

        // get all buttons
        var buttons = $('button');
        // get initial imageURL value
        var initialImageURL = jNode.attr('value');

        //updated picture and give feedback to user
        jNode.on('change', function() {
            // GET new image url on change
            var newImageURL = $(this).val();
            // Get all images on board with the old image
            var targetImages = $(`img[src="${initialImageURL}"]`);

            // ONLY DO SOMETGING IF NEW URL AND NewImageURL not empty
            if (newImageURL !== initialImageURL && newImageURL) {

                // TEST IF IMAGE URL IS VALID IMG
                testImageUrl(newImageURL).then(function(isValidImage) {
                    if (isValidImage) {
                        buttons.prop('disabled', false);
                        jNode.css('color', 'green');
                        // console.log("URL is a valid image.");
                        // SET NEW IMAGE
                        targetImages.attr("src", newImageURL);
                    } else {
                        buttons.prop('disabled', true);
                        jNode.css('color', 'red');
                        jNode.val('Not valid image link!');
                        jNode.attr('value', 'Not a valid image linke!');
                        // console.log("URL is not a valid image.");
                        // REMOVE OLD IMAGE
                        targetImages.remove();
                    }
                });
            } else if (!newImageURL) {
                buttons.prop('disabled', false);
                targetImages.remove();
            }
        });
    }
    waitForKeyElements(onOpenItemImageFieldElement, onImageFieldUpdate);

    /* ###
    FIELDS ON WORKITEM FOUND
       - SET STYLE
       - SET TRANSLATION FOR GIVEN FIELDS
       - SET TRAFFICLIGHT FOR GIVEN FIELDS
     ### */
    function fieldsOnCardFound(jNode) {
        // change fields & edit fields value to right aling
        $('.value, .editor-component').css('text-align', 'right');
        // change fields name to be long
        $('.label').css('overflow', 'visible');

        // Check if translationFields exists and has entries
        const shouldTranslate = translationFields && Object.keys(translationFields).length > 0;
        // Check if planningFlowFields is set and has entries
        const shouldApplyTrafficLightColor = planningFlowFields && planningFlowFields.length > 0;

        // Find all 'div.label.text-ellipsis' elements within jNode
        const labelDivs = jNode[0].querySelectorAll('div.label.text-ellipsis');

        // Process each labelDiv
        labelDivs.forEach(function(labelDiv) {

            // Apply translations to field names
            if (shouldTranslate) {
                translateTileFields(labelDiv, translationFields)
            }

            // Apply trafficlight to field values
            if (shouldApplyTrafficLightColor) {
                planningFlowLogic(labelDiv, planningFlowFields, trafficLightSettings);
            }
        });
    }
    waitForKeyElements(fieldsOnCardDiv, fieldsOnCardFound);

    // ON OPEN ITEM CHANGE DATES
    function onOpenItemChangeDates(jNode) {
        let datetimeValue = jNode.attr('datetime');
        let dateTime = new Date(datetimeValue);
        let correctFormat = dateTime.toLocaleString();
        jNode.text(correctFormat).css('font-weight', 'bold');
    }
    waitForKeyElements(onOpenItemDates, onOpenItemChangeDates);

    function autoUpdate(){
        // check if image exists and image is there, remove image
        var images = $('img.workItemPictures');

        images.each(function() {
            var image = $(this);
            var imageField = image.parent().parent().parent().find(imageFieldLabel);
            var imageURL = imageField.next().text();

            // if imagefield is empty remove old picture
            if (imageField.length === 0) {
                image.remove();
            } else if (image.attr('src') != imageURL) {
                // if image url and image have different URL update it
                image.attr("src", imageURL);
            }
        });
    }

    // Set flagpole to swimlane color
    setFlagPoleToSwimlaneColor();

    // Reload page every one hour
    setInterval(() => window.location.reload(), minutes * 60 * 1000);

})();