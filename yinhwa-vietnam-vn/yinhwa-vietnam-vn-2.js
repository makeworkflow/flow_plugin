// https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/yinhwa-vietnam-vn/yinhwa-vietnam-vn-2.js
// ==UserScript==
// @name         Flow.Plugin 2 - YinHwa Vietnam - VN
// @namespace    https://makeworkflow.de
// @description  Custom FLOW.PLUGIN to have more features on the MS DEVOPS board for increased visibility and usability
// @version      0.0.1
// @run-at       document-end
// @grant        unsafeWindow

// @match        https://dev.azure.com/YinHwa-Vietnam/PRODUCTION
// @match        https://dev.azure.com/YinHwa-Vietnam/PRODUCTION/_boards/board/t/*
// @match        https://dev.azure.com/YinHwa-Vietnam/MODEL%20ROOM
// @match        https://dev.azure.com/YinHwa-Vietnam/MODEL%20ROOM/_boards/board/t/*

// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/jquery.min.js
// @require      https://gitlab.com/makeworkflow/flow_plugin/-/raw/main/dependencies/waitForKeyElements.js

// ==/UserScript==

(function() {
    'use strict';

    // DYNAMIC LANGUAGE DROP DOWN FEATURE
    // Inject styles for the custom dropdown
    const customStyles = `
        .dropdown {
            position: relative;
            display: inline-block;
            z-index: 10000; /* High z-index to ensure it appears above other elements */
        }
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 125px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 10000; /* High z-index to ensure it appears above other elements */
            cursor: pointer;
        }
        .dropdown-content div {
            color: black;
            padding: 8px 12px;
            text-decoration: none;
            display: flex;
            align-items: center;
        }
        .dropdown-content div:hover {
            background-color: #f1f1f1;
        }
        .dropdown:hover .dropdown-content {
            display: block;
        }
        .dropdown-content img {
            margin-right: 10px;
            width: 26px;
            height: 20px;
        }
        .dropbtn {
            background-color: rgba(233,233,233);
            padding: 6.5px 10px;
            font-size: 14px;
            border: none;
            cursor: pointer;
            display: flex;
            width: 135px;  // Fixed width for consistent button size
            text-align: center;
            align-items: center;
            justify-content: center;  /* Add this line */
            font-weight: 600;
            margin-top: 4px;
        }
        .dropbtn img {
            margin-right: 5px;
            width: 26px;
            height: 20px;
        }
        .dropbtn:focus {
            outline: none;
        }
    `;
    $('head').append('<style>' + customStyles + '</style>');

    // Add the dropdown HTML
    const dropdownHtml = `
        <div class="dropdown">
            <button class="dropbtn">
                <img src="https://flagcdn.com/64x48/gb.png" alt="English">
                English
            </button>
            <div class="dropdown-content">
                <div data-lang="english">
                    <img src="https://flagcdn.com/64x48/gb.png" alt="English">
                    English
                </div>
                <div data-lang="chinese">
                    <img src="https://flagcdn.com/64x48/cn.png" alt="Chinese">
                    Chinese
                </div>
                <div data-lang="vietnamese">
                    <img src="https://flagcdn.com/64x48/vn.png" alt="Vietnamese">
                    Vietnamese
                </div>
            </div>
        </div>
    `;

    const headerTitleAreas = $('.bolt-header-title-area.flex-column.flex-grow.scroll-hidden');
    if (headerTitleAreas.length > 1) {
        $(headerTitleAreas[1]).after(dropdownHtml);
    }

    // Event listener for language selection
    $('.dropdown-content div').on('click', function() {
        const selectedLang = $(this).data('lang');
        const selectedText = $(this).text().trim();
        const selectedImgSrc = $(this).find('img').attr('src');
        setTranslationLayer(selectedLang);
        saveLanguageSetting(selectedLang); // Save the selected language

        // Update the button to show the selected value and image
        $('.dropbtn').html(`<img src="${selectedImgSrc}" alt="${selectedText}"> ${selectedText}`);
    });

    // Current translation layer
    let currentTranslationLayer = loadLanguageSetting();

    // Translation layers
    const translationLayers = {
        english: {
            "ORDER DATE": "ORDER DATE",
            "CUSTOMER": "CUSTOMER",
            "BRAND": "BRAND",
            "MODEL NUMBER": "MODEL NO",
            "LAST CODE": "LAST CODE",
            "SIZE_": "SIZE",
            "ORDER QTY": "ORDER QTY",
            "WIP QTY": "WIP QTY",
            "ETD": "ETD"
        },
        chinese: {
            "ORDER DATE": "订单日期",
            "CUSTOMER": "客户",
            "BRAND": "品牌",
            "MODEL NUMBER": "模子编号",
            "LAST CODE": "楦头编号",
            "SIZE_": "码数",
            "ORDER QTY": "订单数量",
            "WIP QTY": "本站数量",
            "ETD": "交货日期"
        },
        vietnamese: {
            "ORDER DATE": "Ngày đặt",
            "CUSTOMER": "Khách hàng",
            "BRAND": "Nhãn hiệu",
            "MODEL NUMBER": "MODEL NO",
            "LAST CODE": "Mã phom",
            "SIZE_": "SIZE",
            "ORDER QTY": "Tổng số lượng",
            "WIP QTY": "Số đang SX",
            "ETD": "ETD"
        }
    };

    // Updated translation array
    const stepsTranslationLayer = {
        english: {
            "ORDER APPROVAL": "ORDER APPROVAL",
            "PREPARATION": "PREPARATION",
            "PR MODEL MAKING": "PR MODEL MAKING",
            "MANUAL MODEL MAKING": "MANUAL MODEL MAKING",
            "PR CAD 3D & SCANNING": "PR CAD 3D & SCANNING",
            "CAD 3D & SCANNING": "CAD 3D & SCANNING",
            "PR CAD 2D": "PR CAD 2D",
            "CAD 2D": "CAD 2D",
            "MR APPROVAL": "MR APPROVAL",
            "DONE": "DONE",
            "SALES APPROVAL": "SALES APPROVAL",
            "PR PRODUCTION": "PR PRODUCTION",
            "PR ROUGHING": "PR ROUGHING",
            "ROUGHING": "ROUGHING",
            "PR CUTTING / CONJUNCTION": "PR CUTTING / CONJUNCTION",
            "CUTTING / CONJUNCTION": "CUTTING / CONJUNCTION",
            "PR MOD": "PR MOD",
            "MOD": "MOD",
            "PR SDF": "PR SDF",
            "SDF": "SDF",
            "PR HEEL/TOE FINISHING": "PR HEEL/TOE FINISHING",
            "HEEL/TOE FINISHING": "HEEL/TOE FINISHING",
            "PR STAMPING": "PR STAMPING",
            "STAMPING": "STAMPING",
            "PR PIN MARK": "PR PIN MARK",
            "PIN MARK": "PIN MARK",
            "PR QC": "PR QC",
            "QC": "QC",
            "PR PACKING": "PR PACKING",
            "PACKING": "PACKING",
            "FG WAREHOUSE": "FG WAREHOUSE",
            "SHIPPED": "SHIPPED",
        },
        chinese: {
            "ORDER APPROVAL": "订单确认",
            "PREPARATION": "准备纸板",
            "PR MODEL MAKING": "待模子制作",
            "MANUAL MODEL MAKING": "模子制作中",
            "PR CAD 3D & SCANNING": "待3D作业和扫描",
            "CAD 3D & SCANNING": "3D作业和扫描",
            "PR CAD 2D": "待2D作业",
            "CAD 2D": "2D作业中",
            "MR APPROVAL": "模具室审核",
            "DONE": "完成",
            "SALES APPROVAL": "业务确认",
            "PR PRODUCTION": "待生产",
            "PR ROUGHING": "待粗车",
            "ROUGHING": "粗车",
            "PR CUTTING / CONJUNCTION": "待结合",
            "CUTTING / CONJUNCTION": "结合",
            "PR MOD": "待传统细车",
            "MOD": "传统细车",
            "PR SDF": "待SDF细车",
            "SDF": "SDF细车",
            "PR HEEL/TOE FINISHING": "等待磨头尾",
            "HEEL/TOE FINISHING": "磨头尾",
            "PR STAMPING": "待盖印",
            "STAMPING": "盖印",
            "PR PIN MARK": "待做记号",
            "PIN MARK": "做记号",
            "PR QC": "待品检",
            "QC": "品检",
            "PR PACKING": "待包装",
            "PACKING": "完成包装",
            "FG WAREHOUSE": "成品仓库",
            "SHIPPED": "完成出货",
        },
        vietnamese: {
            "ORDER APPROVAL": "Xác nhận đơn",
            "PREPARATION": "CB rập",
            "PR MODEL MAKING": "CB tạo mẫu",
            "MANUAL MODEL MAKING": "Đang tạo mẫu",
            "PR CAD 3D & SCANNING": "CB 3D&Scan",
            "CAD 3D & SCANNING": "Đang 3D&Scan",
            "PR CAD 2D": "CB 2D",
            "CAD 2D": "Đang làm 2D",
            "MR APPROVAL": "MR xác nhận",
            "DONE": "Hoàn thành",
            "SALES APPROVAL": "Nghiệp vụ xác nhận",
            "PR PRODUCTION": "CB sản xuất",
            "PR ROUGHING": "CB chạy nhám",
            "ROUGHING": "Đang chạy nhám",
            "PR CUTTING / CONJUNCTION": "CB kết hợp",
            "CUTTING / CONJUNCTION": "Đang làm kết hợp",
            "PR MOD": "CB bóng (NL)",
            "MOD": "Đang bóng (NL)",
            "PR SDF": "CB bóng (F4)",
            "SDF": "Đang bóng (F4)",
            "PR HEEL/TOE FINISHING": "CB mài đầu gót",
            "HEEL/TOE FINISHING": "Mài đầu gót",
            "PR STAMPING": "CB đóng dấu",
            "STAMPING": "Đóng dấu",
            "PR PIN MARK": "CB làm kí hiệu",
            "PIN MARK": "Đang làm kí hiệu",
            "PR QC": "CB kiểm phẩm",
            "QC": "Đang KP",
            "PR PACKING": "CB đóng gói",
            "PACKING": "Đang đóng gói",
            "FG WAREHOUSE": "Đang ở kho",
            "SHIPPED": "Đã Xuất hàng",
        }
    };

    function saveLanguageSetting(language) {
        localStorage.setItem('selectedLanguage', language);
    }

    function loadLanguageSetting() {
        return localStorage.getItem('selectedLanguage') || 'english';
    }

    // Function to set the translation layer
    function setTranslationLayer(language) {
        currentTranslationLayer = language;
        refreshTranslations();
    }

    // Function to refresh translations
    function refreshTranslations() {
        const fields = $('div.fields').find('div.label.text-ellipsis');
        fields.each(function() {
            const labelDiv = this;
            translateTileFields(labelDiv, translationLayers[currentTranslationLayer]);
        });

        const headers = $('.kanban-board-column-header');
        headers.each(function() {
            const header = $(this);
            translateHeaderSteps(header, stepsTranslationLayer[currentTranslationLayer]);
        });
    }

    // FIELDS ON WORKITEM FOUND
    function fieldsOnCardFound(jNode) {
        $('.value, .editor-component').css('text-align', 'right');
        $('.label').css('overflow', 'visible');

        const shouldTranslate = translationLayers[currentTranslationLayer] && Object.keys(translationLayers[currentTranslationLayer]).length > 0;

        const labelDivs = jNode[0].querySelectorAll('div.label.text-ellipsis');

        labelDivs.forEach(function(labelDiv) {
            if (shouldTranslate) {
                translateTileFields(labelDiv, translationLayers[currentTranslationLayer]);
            }
        });
    }

    // Add dropdown to the document once
    waitForKeyElements('div.fields', fieldsOnCardFound);

    function translateTileFields(labelDiv, translationFields) {
        if (typeof labelDiv.textContent === "string") {
            if (!labelDiv.getAttribute('data-original-text')) {
                labelDiv.setAttribute('data-original-text', labelDiv.textContent);
            }
            const originalText = labelDiv.getAttribute('data-original-text').toUpperCase();
            if (translationFields[originalText]) {
                labelDiv.textContent = translationFields[originalText];
            }
        }
    }

    function translateHeaderSteps(header, translationFields) {
        const ariaLabel = header.attr('aria-label');
        if (ariaLabel) {
            let span = header.find('span').filter(function() {
                return $(this).text().toUpperCase() === ariaLabel.toUpperCase();
            }).first(); // Find the first matching span

            if (!span.length) {
                // If no span matches the aria-label, check against data-original-text
                span = header.find('span').filter(function() {
                    return $(this).attr('data-original-text') && $(this).attr('data-original-text').toUpperCase() === ariaLabel.toUpperCase();
                }).first();
            }

            if (span.length) {
                if (!span.attr('data-original-text')) {
                    span.attr('data-original-text', span.text());
                }
                const originalText = span.attr('data-original-text').toUpperCase();
                if (translationFields[originalText]) {
                    span.text(translationFields[originalText]);
                }
            }
        }
    }

    $(document).ready(function() {
        const languages = {
        'chinese': {
            text: 'Chinese',
            imgSrc: 'https://flagcdn.com/64x48/cn.png'
        },
        'vietnamese': {
            text: 'Vietnamese',
            imgSrc: 'https://flagcdn.com/64x48/vn.png'
        },
        'english': {
            text: 'English',
            imgSrc: 'https://flagcdn.com/64x48/gb.png'
        }
    };
        const selectedLang = loadLanguageSetting();
        const selected = languages[selectedLang] || languages.English; // Default to English if not found

        $('.dropbtn').html(`<img src="${selected.imgSrc}" alt="${selected.text}"> ${selected.text}`);
        setTranslationLayer(selectedLang); // Set the initial translation layer
    });

    // Wait for the kanban board column headers to be available
    waitForKeyElements('.kanban-board-column-header', function() {
        refreshTranslations();
    });

})();
